/* shell.c       Copyright (c) 2000-2003 Nagy Daniel
 *
 * $Date: 2003/11/11 13:55:44 $
 * $Revision: 1.2 $
 *
 * This module spawns a DOS shell
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#if defined (__WATCOMC__)
 #include <graph.h>
 #include <direct.h>
#elif defined (__DJGPP__)
 #include <unistd.h>
#endif

#include "xmalloc.h"
#include "vidio.h"


/*
 * Spawn a DOS shell
 */
void Shell(void)
{
char *comspec;
char olddir[80];
char *p;
#if defined (__WATCOMC__)
 struct rccoord pos;
#elif defined (__DJGPP__)
 int x,y;
#endif

#if defined (__WATCOMC__)
   pos = _gettextposition();
#elif defined (__DJGPP__)
   x = wherex();
   y = wherey();
#endif

   p = savescreen();
   getcwd(olddir, sizeof(olddir));
   cputs("\r\n\r\nType EXIT to quit from this shell\r\n");
   comspec = getenv("COMSPEC");
   system(comspec);
   chdir(olddir);
   restorescreen(p);

#if defined (__WATCOMC__)
   _settextposition(pos.row, pos.col);
#elif defined (__DJGPP__)
    gotoxy(x, y);
#endif
}
