#ifndef _RSA_H
#define _RSA_H

#include "sshbn.h"

/* RSA key structure */

typedef struct {
    unsigned short bits;
    unsigned short bytes;
    Bignum modulus;
    Bignum exponent;
    Bignum private_exponent;
    Bignum p;
    Bignum q;
    Bignum iqmp;
    char *comment;
} RSAKey;

extern int makekey(unsigned char *, RSAKey *, char **, short);
extern int makeprivate(unsigned char *, RSAKey *);
extern void rsaencrypt(char *, int, RSAKey *);
extern Bignum rsadecrypt(Bignum, RSAKey *);
extern int rsa_verify(RSAKey *);
extern void freersakey(RSAKey *);
extern int loadrsakey(char *, RSAKey *, char *);
extern int rsakey_encrypted(char *, char **);
extern int key_type(char *);
extern char *key_type_to_str(int);

#endif
