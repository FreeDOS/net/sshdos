#ifndef _XMALLOC_H
#define _XMALLOC_H

#include <stdlib.h>

extern void* xmalloc(size_t size);
extern void* xrealloc(void *p, size_t size);
extern void xfree(void *p);

#endif
