#ifndef _TRANSPRT_H
#define _TRANSPRT_H

/* Packet structure */

typedef struct {
    unsigned long length;	/* type + body length */
    unsigned long maxlen;	/* max length */
    unsigned char type;		/* packet type */
    char *whole;		/* pointer to whole packet */
    char *body;			/* pointer to useful data */
    char *ptr;			/* sliding pointer in body */
} Packet;

/* request compression from server */
extern void Request_Compression(int);

/* free compression memory */
extern void Disable_Compression(void);

/* get a packet from the transport layer */
extern void SSH_pkt_Read(unsigned char);
extern void SSH_pkt_Read_SucFail(void);

/* create header for raw outgoing packet */
extern void SSH_pkt_init(unsigned char);

/* create outgoing packet */
extern void SSH_pkt_send(void);

/* SSH1 packet assembly */
extern void SSH_putuint32(unsigned long);
extern void SSH_putbool(char);
extern void SSH_putstring(char *);
extern void SSH_putdata(char *, unsigned short);
extern void SSH_putbn(unsigned short *);

extern void SendSSHPacket(char *, unsigned short);

/* SSH1 disconnect */
extern void SSH_Disconnect(const char *fmt, ...);

#endif
