#ifndef _AUTH_H
#define _AUTH_H

extern short SSH1_Send_User_Name(char *name);
extern short SSH1_Auth_Pubkey(char *pubkey);
extern void SSH1_Auth_Password(char *password);

#endif
