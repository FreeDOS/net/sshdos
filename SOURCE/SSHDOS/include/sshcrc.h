#ifndef _SSHCRC_H
#define _SSHCRC_H

/* CRC generation */
extern unsigned long sshcrc32(const void *, size_t);

#endif
