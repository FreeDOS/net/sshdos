#ifndef _SSH_H
#define _SSH_H

/* SSH constants and protocol structure */

#define MAX_PACKET_SIZE 8192	/* max packet size */
#define EXIT_SSH 1381

#define SSH_PORT 22

#define SSH_MSG_NONE                            0       /* no message */
#define SSH_MSG_DISCONNECT                      1       /* cause (string) */
#define SSH_SMSG_PUBLIC_KEY                     2       /* ck,msk,srvk,hostk */
#define SSH_CMSG_SESSION_KEY                    3       /* key (MP_INT) */
#define SSH_CMSG_USER                           4       /* user (string) */
#define SSH_CMSG_AUTH_RHOSTS                    5       /* user (string) */
#define SSH_CMSG_AUTH_RSA                       6       /* modulus (MP_INT) */
#define SSH_SMSG_AUTH_RSA_CHALLENGE             7       /* int (MP_INT) */
#define SSH_CMSG_AUTH_RSA_RESPONSE              8       /* int (MP_INT) */
#define SSH_CMSG_AUTH_PASSWORD                  9       /* pass (string) */
#define SSH_CMSG_REQUEST_PTY                    10      /* TERM, tty modes */
#define SSH_CMSG_WINDOW_SIZE                    11      /* row,col,xpix,ypix */
#define SSH_CMSG_EXEC_SHELL                     12      /* */
#define SSH_CMSG_EXEC_CMD                       13      /* cmd (string) */
#define SSH_SMSG_SUCCESS                        14      /* */
#define SSH_SMSG_FAILURE                        15      /* */
#define SSH_CMSG_STDIN_DATA                     16      /* data (string) */
#define SSH_SMSG_STDOUT_DATA                    17      /* data (string) */
#define SSH_SMSG_STDERR_DATA                    18      /* data (string) */
#define SSH_CMSG_EOF                            19      /* */
#define SSH_SMSG_EXITSTATUS                     20      /* status (int) */

#define SSH_MSG_IGNORE                          32      /* string */
#define SSH_CMSG_EXIT_CONFIRMATION              33      /* */
#define SSH_MSG_DEBUG                           36      /* string */
#define SSH_CMSG_REQUEST_COMPRESSION            37      /* level 1-9 (int) */
#define SSH_CMSG_MAX_PACKET_SIZE                38      /* max_size (int) */

/* Major protocol version.  Different version indicates major incompatiblity
   that prevents communication.  */
#define PROTOCOL_MAJOR          1

/* Minor protocol version.  Different version indicates minor incompatibility
   that does not prevent interoperation. */
#define PROTOCOL_MINOR          5

#define SSH_CIPHER_3DES		3
#define SSH_CIPHER_BLOWFISH	6

enum {
    SSH_KEYTYPE_UNOPENABLE,
    SSH_KEYTYPE_UNKNOWN,
    SSH_KEYTYPE_SSH1, SSH_KEYTYPE_SSH2,
    SSH_KEYTYPE_OPENSSH, SSH_KEYTYPE_SSHCOM
};

/* Authentication mask bits */
#define SSH_AUTH_RSA		2
#define SSH_AUTH_PASSWORD	3

/* SCP */
#define TRANSBUF_SIZE 8192
#define SCP_SINK_FILE   1
#define SCP_SINK_DIR    2
#define SCP_SINK_ENDDIR 3
#define SCP_SINK_RETRY  4	       /* not an action; just try again */

#endif
