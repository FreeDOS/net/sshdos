#ifndef _CIPHER_H
#define _CIPHER_H

/* Cipher functions */

typedef void (*SessKeyInitPtr)(unsigned char *);
typedef void (*CryptPtr)(unsigned char *, unsigned int);

extern void blowfish_sesskey(unsigned char *);
extern void blowfish_encrypt_blk(unsigned char *, unsigned int);
extern void blowfish_decrypt_blk(unsigned char *, unsigned int);

extern void des3_sesskey(unsigned char *);
extern void des3_encrypt_blk(unsigned char *, unsigned int);
extern void des3_decrypt_blk(unsigned char *, unsigned int);

extern void des3_decrypt_pubkey(unsigned char *, unsigned char *, int);

#endif
