/* xmalloc.c       Copyright (c) 2000-2003 Nagy Daniel
 *
 * $Date: 2003/11/11 13:55:44 $
 * $Revision: 1.2 $
 *
 * Safe malloc with NULL check
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stdlib.h>

#include "common.h"

/*
 * xmalloc - goes fatal if allocation is unsuccessful
 */
void *xmalloc(size_t size)
{
void *tmp;

   if((tmp = malloc(size)) == NULL)
      fatal("Memory allocation error");
   return tmp;
}

/*
 * xrealloc - goes fatal if reallocation is unsuccessful
 */
void* xrealloc(void* ptr, size_t size)
{
void *tmp;

   if((tmp = realloc(ptr, size)) == NULL)
	fatal("Memory reallocation error");
   return tmp;
}

/*
 * xfree - don't free if NULL
 */
void xfree(void *ptr)
{
   if(ptr != NULL)
      free(ptr);
}
