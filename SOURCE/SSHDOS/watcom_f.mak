#
# OpenWatcom makefile for SSHDOS (protected mode version)
#

# Where is your compiler installed?
COMPPATH=c:\watcom

# Where is WATT-32?
WATT32=c:\watt32

# Debug
DEBUG=-d2

# uncomment this for B&W mode
COLOR = -DCOLOR

#################################################################
# In normal cases, no other settings should be changed below!!! #
#################################################################

CC = wcc386
LINKER = wlink

CFLAGS = -mf -3r -fp3 -s $(DEBUG) -i=$(COMPPATH)\h;.\include;$(WATT32)\inc $(COLOR) -DSSH1

.C.OBJ:	
        $(CC) $(CFLAGS) $[@

LIBS = lib\misc.lib lib\crypto.lib lib\ssh.lib lib\vt100.lib lib\wattcpwf.lib lib\zlib.lib

all:    sshd386.exe scpd386.exe tel386.exe

sshd386.exe : sshdos.obj $(LIBS)
	$(LINKER) @sshd386.lnk

scpd386.exe : scpdos.obj $(LIBS)
	$(LINKER) @scpd386.lnk

tel386.exe : telnet.obj lib/misc.lib lib/wattcpwf.lib lib/vt100.lib
	$(LINKER) @tel386.lnk

lib\crypto.lib: sshcrc.obj sshrsa.obj sshblowf.obj sshdes.obj sshmd5.obj sshbn.obj sshpubk.obj
	wlib -b -c lib\crypto.lib -+sshcrc.obj -+sshrsa.obj -+sshblowf.obj -+sshdes.obj -+sshmd5.obj
	wlib -b -c lib\crypto.lib -+sshbn.obj -+sshpubk.obj

lib\ssh.lib: negotiat.obj transprt.obj auth.obj
	wlib -b -c lib\ssh.lib -+negotiat.obj -+transprt.obj -+auth.obj

lib\vt100.lib: vttio.obj vidio.obj keyio.obj keymap.obj
	wlib -b -c lib\vt100.lib -+vttio.obj -+vidio.obj -+keyio.obj -+keymap.obj

lib\misc.lib: xmalloc.obj common.obj shell.obj
	wlib -b -c lib\misc.lib -+xmalloc.obj -+common.obj -+shell.obj

clean: .SYMBOLIC
	del *.obj
	del lib\vt100.lib
	del lib\crypto.lib
	del lib\misc.lib
	del lib\ssh.lib
	del sshd386.exe
	del scpd386.exe
	del tel386.exe
