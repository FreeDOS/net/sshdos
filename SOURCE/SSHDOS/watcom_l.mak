#
# OpenWatcom makefile for SSHDOS (real mode - large)
#

# Where is your compiler installed?
COMPPATH=c:\watcom

# Where is WATT32?
WATT32=c:\watt32

# Debug
DEBUG=-d2

# uncomment this for B&W mode
COLOR = -DCOLOR

#################################################################
# In normal cases, no other settings should be changed below!!! #
#################################################################

CC = wcc
LINKER = wlink

CFLAGS = -ml -0 -fpi -bt=dos -s $(DEBUG) -i=$(COMPPATH)\h;include;$(WATT32)\inc $(COLOR) -DSSH1

.C.OBJ:	
        $(CC) $(CFLAGS) $[@

LIBS = lib\misc.lib lib\crypto.lib lib\ssh.lib lib\vt100.lib lib\wattcpwl.lib lib\zlib.lib

all:    sshdos.exe scpdos.exe telnet.exe

sshdos.exe : sshdos.obj $(LIBS)
	$(LINKER) @sshdos.lnk

scpdos.exe : scpdos.obj $(LIBS)
	$(LINKER) @scpdos.lnk

telnet.exe : telnet.obj lib/misc.lib lib/wattcpwl.lib lib/vt100.lib
	$(LINKER) @telnet.lnk

lib\crypto.lib: sshcrc.obj sshrsa.obj sshblowf.obj sshdes.obj sshmd5.obj sshbn.obj sshpubk.obj
	wlib -b -c lib\crypto.lib -+sshcrc.obj -+sshrsa.obj -+sshblowf.obj -+sshdes.obj -+sshmd5.obj
	wlib -b -c lib\crypto.lib -+sshbn.obj -+sshpubk.obj

lib\ssh.lib: negotiat.obj transprt.obj auth.obj
	wlib -b -c lib\ssh.lib -+negotiat.obj -+transprt.obj -+auth.obj

lib\vt100.lib: vttio.obj vidio.obj keyio.obj keymap.obj
	wlib -b -c lib\vt100.lib -+vttio.obj -+vidio.obj -+keyio.obj -+keymap.obj

lib\misc.lib: xmalloc.obj common.obj shell.obj
	wlib -b -c lib\misc.lib -+xmalloc.obj -+common.obj -+shell.obj

clean: .SYMBOLIC
	del *.obj
	del lib\vt100.lib
	del lib\crypto.lib
	del lib\misc.lib
	del lib\ssh.lib
	del sshdos.exe
	del scpdos.exe
	del telnet.exe
